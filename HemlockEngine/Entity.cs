﻿using System;
using System.Collections.Generic;

namespace HemlockEngine
{
    public class Entity
    {
        EntityManager entityManager;
        Dictionary<Type, Component> componentMap;
        public HashSet<Type> componentTypes;

        public uint id { get; set; }

        public Entity(EntityManager entityManager, uint id)
        {
            this.id = id;
            this.entityManager = entityManager;
            componentMap = new Dictionary<Type, Component>();
            componentTypes = new HashSet<Type>();
        }

        public T AddComponent<T>() where T : Component, new()
        {
            T component = entityManager.CreateComponent<T>(this);
            componentMap.Add(typeof(T), component);
            componentTypes.Add(component.GetType());
            entityManager.CheckAndRegisterEntity(this);
            return component;
        }

        public T GetComponent<T>() where T : Component
        {
            return (T)componentMap[typeof(T)];
        }

        public bool HasComponent(Type componentType)
        {
            return componentTypes.Contains(componentType);
        }
    }
}
