﻿using System.Collections.Generic;
using System.Linq;

namespace HemlockEngine
{
    public class EntityManager
    {
        uint nextID = 0;

        List<Entity> entities = new List<Entity>();
        List<Engine> engines = new List<Engine>();

        Dictionary<uint, Entity> entityIDMap = new Dictionary<uint, Entity>();
        Dictionary<System.Type, List<uint>> componentEntityIDMap = new Dictionary<System.Type, List<uint>>();

        public IEnumerable<Entity> GetEntitiesWithComponents(params System.Type[] componentTypes)
        {
            return componentTypes.Select((arg) => componentEntityIDMap[arg])
                                 .Aggregate((IEnumerable<uint> arg1, IEnumerable<uint> arg2) => arg1.Intersect(arg2))
                                 .Select(arg1 => FindEntity(arg1));
        }

        public EntityManager()
        {
            InitializeComponentEntityIDMap();
        }

        public Entity FindEntity(uint id)
        {
            return entityIDMap[id];
        }

        public Entity CreateEntity()
        {
            Entity newEntity = new Entity(this, nextID);
            entities.Add(newEntity);
            entityIDMap.Add(nextID, newEntity);
            nextID += 1;
            return newEntity;
        }

        public T CreateComponent<T>(Entity entity) where T : Component, new()
        {
            T component = new T();
            component.Entity = entity;
            componentEntityIDMap[typeof(T)].Add(entity.id);
            return component;
        }

        public void RegisterEngine(Engine engine)
        {
            engines.Add(engine);
        }

        public void CheckAndRegisterEntity(Entity entity)
        {
            foreach (Engine engine in engines)
            {
                engine.CheckAndRegisterEntity(entity);
            }
        }

        void InitializeComponentEntityIDMap()
        {
            IEnumerable<System.Type> listOfComponentTypes = (from domainAssembly in System.AppDomain.CurrentDomain.GetAssemblies()
                                                             from assemblyType in domainAssembly.GetTypes()
                                                             where assemblyType.IsSubclassOf(typeof(Component))
                                                             select assemblyType);

            foreach (var b in listOfComponentTypes)
            {
                componentEntityIDMap.Add(b, new List<uint>());
            }
        }
    }
}
